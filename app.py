from flask import Flask, render_template, jsonify
import json
import socket
# from fastapi import FastAPI


app = Flask(__name__)


def fetchDetails():
    # IP lookup from hostname
    hostname = socket.gethostname()
    ip = socket.gethostbyname(hostname)
    return str(hostname), str(ip)


@app.route("/health")
def checkHelth():
    return jsonify(status="up")


@app.route("/", methods=["GET", "POST"])
def hello_World():
    hostname, ip = fetchDetails()
    return render_template("index.html", hostname=hostname, ip=ip)


if __name__ == "__main__":
    app.run("0.0.0.0", debug=True)
